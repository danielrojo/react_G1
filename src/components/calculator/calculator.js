import React, { Component } from 'react';
import './calculator.css';
import Display from './display';
import Keyboard from './keyboard';

const INIT_STATE = 0;
const FIRST_FIGURE_STATE = 1;
const SECOND_FIGURE_STATE = 2;
const RESULT_STATE = 3;

export default class Calculator extends Component {


    constructor(props) {
        super(props);
        console.log('Data: ' + JSON.stringify(props.data));
        this.calculatorState = this.props.data.calculatorState;
        this.firstFigure = this.props.data.firstFigure;
        this.secondFigure = this.props.data.secondFigure;
        this.result = this.props.data.result;
        this.operator = this.props.data.operator;

        this.state = {
            display: this.props.data.display
        }
    }

    handleFigure(number) {
        switch (this.calculatorState) {
            case INIT_STATE:
                this.firstFigure = number;
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: this.state.display + number.toString() })
                break;
            case FIRST_FIGURE_STATE:
                this.firstFigure = this.firstFigure * 10 + number;
                this.setState({ display: this.state.display + number.toString() })
                break;

            case SECOND_FIGURE_STATE:
                this.secondFigure = this.secondFigure * 10 + number;
                this.setState({ display: this.state.display + number.toString() })
                break;

            case RESULT_STATE:
                this.result = 0;
                this.secondFigure = 0;
                this.operator = '';
                this.firstFigure = number;
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: number });
                break;
            default:
                break;
        }
        this.props.updateCalculator({
            calculatorState: this.calculatorState,
            firstFigure: this.firstFigure,
            secondFigure: this.secondFigure,
            result: this.result,
            operator: this.operator,
            display: this.state.display
        });
    }

    handleSymbol(symbol) {
        switch (this.calculatorState) {
            case INIT_STATE:
                break;
            case FIRST_FIGURE_STATE:
                if (this.isOperator(symbol)) {
                    this.operator = symbol;
                    this.setState({ display: this.state.display + symbol });
                    this.calculatorState = SECOND_FIGURE_STATE;
                }
                break;

            case SECOND_FIGURE_STATE:
                if (symbol === '=') {
                    this.result = this.resolve();
                    this.setState({ display: this.state.display + symbol + this.result.toString() })
                    console.log(this.result);
                    this.calculatorState = RESULT_STATE;
                }
                break;

            case RESULT_STATE:
                if (this.isOperator(symbol)) {
                    this.firstFigure = this.result;
                    this.operator = symbol;
                    this.secondFigure = 0;
                    this.result = 0;
                    this.setState({ display: this.firstFigure.toString() + this.operator });
                    this.calculatorState = SECOND_FIGURE_STATE;
                }
                break;
            default:
                break;
        }
        this.props.updateCalculator({
            calculatorState: this.calculatorState,
            firstFigure: this.firstFigure,
            secondFigure: this.secondFigure,
            result: this.result,
            operator: this.operator,
            display: this.state.display
        });
    }

    resolve() {
        console.log(this.operator);
        switch (this.operator) {
            case '+':
                return this.firstFigure + this.secondFigure;
            case '-':
                return this.firstFigure - this.secondFigure;
            case 'x':
                return this.firstFigure * this.secondFigure;
            case '/':
                return this.firstFigure / this.secondFigure;
            default:
                console.log('Entra en default');
                break;
        }
    }

    isOperator(symbol) {
        return (symbol === '+' || symbol === '-' || symbol === 'x' || symbol === '/');
    }

    handleClick(value) {

        if (typeof value === 'number') {
            this.handleFigure(value);
        } else if (typeof value === 'string') {
            this.handleSymbol(value);
        }
    }

    render() {
        return (
            <div className="container mt-2">
                <table border="1">
                    <thead>
                        <tr>
                            <Display input={this.state.display} />
                            <td><input type="button" value="c" /> </td>
                        </tr>
                    </thead>
                    <Keyboard signal={(keypressed) => { this.handleClick(keypressed) }} />
                </table>
            </div>
        )
    }
}
