import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Display extends Component {

    static propTypes = {
        input: PropTypes.string.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <td colSpan="3"><p>{this.props.input}</p></td>
        )
    }
}


