import React, { Component } from 'react'

export default class FormHeroes extends Component {
    constructor(props) {
        super(props)

        this.state = {
            newHero: this.props.hero,
            showField: 'alert alert-danger alert-dismissible fade show'
        }
    }

    handleChange(event) {
        let myHero = this.state.newHero;
        if (event.target.name === 'nameInput') {
            myHero.name = event.target.value;
        } else if (event.target.name === 'descriptionInput') {
            myHero.description = event.target.value;
        }
        console.log(event.target.validity.valid);
        if (event.target.validity.valid) {
            this.setState({ showField: 'alert alert-danger alert-dismissible fade' })
        } else {
            this.setState({ showField: 'alert alert-danger alert-dismissible fade show' })
        }

        this.setState({ newHero: myHero });
        this.props.updateForm(myHero);
    }

    addHero() {
        this.props.newHero(this.state.newHero);
        this.setState({ newHero: { name: '', description: '' } })
        this.props.updateForm({ name: '', description: '' });
    }

    render() {
        return (
            <div className="form-group">
                <label for="">Nombre del héroe</label>
                <form>
                    <input type="text" className="form-control" name="nameInput" id="" aria-describedby="helpId" placeholder="" value={this.state.newHero.name} onChange={(e) => { this.handleChange(e) }} required={true} />
                    <div className={this.state.showField} role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>El nombre está mal</strong>
                    </div>
                    <label for="">Descripción del héroe</label>
                    <input type="text" className="form-control" name="descriptionInput" id="" aria-describedby="helpId" placeholder="" value={this.state.newHero.description} onChange={(e) => { this.handleChange(e) }} />
                    <button type="button" className="btn btn-primary" onClick={() => this.addHero()}>Añade héroe</button>
                </form>
            </div>
        )
    }
}
