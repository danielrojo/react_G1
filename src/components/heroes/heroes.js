import React, { Component } from 'react'
import FormHeroes from './formHeroes';
import ListHeroes from './listHeroes';

export default class Heroes extends Component {



    constructor(props) {
        super(props)

        this.state = {
            heroes: this.props.heroes
        }

    }

    addHero(hero) {
        this.setState({ heroes: [...this.state.heroes, {...hero}] });
        this.props.addHero({...hero});
    }
    
    removeHero(i) {
        let myHeroes = [...this.state.heroes];
        myHeroes.splice(i,1);
        this.setState({heroes: myHeroes})
        this.props.removeHero(i);
    }

    render() {

        return (
            <div class="container">
                <FormHeroes newHero={(hero)=>{this.addHero(hero)}} hero={this.props.newHero} updateForm={(hero)=>this.props.updateHero(hero)}/>
                <ListHeroes heroes={this.state.heroes} removeSignal={(index)=>this.removeHero(index)}/>

            </div>

        )
    }
}
