import React, { Component } from 'react'

export default class ListHeroes extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }

    removeHero(i) {
        this.props.removeSignal(i);
    }
    
    render() {

        const listHeroes = this.props.heroes.map(
            (hero, i) => <li key={i} className="list-group-item"><strong>{hero.name}</strong><br/>{hero.description} <button name="" id="" class="btn btn-danger float-right"  onClick={()=>{this.removeHero(i)}}>eliminar</button></li>)

        return (
            <ul className="list-group">
                {listHeroes}
            </ul>
        )
    }
}
