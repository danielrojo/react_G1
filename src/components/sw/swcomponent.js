import React, { useState, useEffect } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';


function SwComponent({ myHeroes, updateHeroes }) {

    const [data, setData] = useState([]);
    const [heroes, setHeroes] = useState(myHeroes);
    const [status, setStatus] = useState('idle');
    const [query, setQuery] = useState('https://swapi.dev/api/people');

    const myFetch = () => {
        if (status !== 'idle') return;

        const fetchData = async () => {
            console.log('Fetching...');
            setStatus('fetching');
            const response = await fetch(
                query
            );
            const data = await response.json();
            setData(data);
            setHeroes([...heroes, ...data.results])
            updateHeroes({heroes: heroes, next: data.next});
            setStatus('idle');
        };

        fetchData();
    };

    useEffect(myFetch, [query]);

    const handleAddHeroes = () => {
        setQuery(data.next);
    }

    const displayHeroes = heroes.map((hero, index) => {
        return (
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">{hero.name}</h4>
                        <p className="card-text">{hero.height}</p>
                        <p className="card-text">{hero.weight}</p>
                    </div>
                </div>
            </div>)
    });

    return (
        <div class="container">
            <div className="row">
                {displayHeroes}
            </div>
            <button type="button" class="btn btn-primary" onClick={handleAddHeroes}>más</button>
        </div>

    )
}

export default SwComponent
