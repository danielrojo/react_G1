import React from 'react'
import Slider from '@material-ui/core/Slider';

function RangBeers(props) {
    const [value, setValue] = React.useState(props.range);

    const handleChange = (event, newValue) => {
        setValue(newValue);
        props.handleRange(newValue);
    };

    return (
        <Slider
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        max={50}
        min={0}
        step={0.1}
    />
    )
}

export default RangBeers
