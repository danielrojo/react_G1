import React from 'react'

function BeersList(props) {


    const myBeers = props.beers.map((beer, index) => {
        return (
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">{beer.name}</h4>
                        <img className="card-img-top mx-auto d-block" src={beer.image_url} style={{ width: '45px' }} alt="" />
                        <p className="card-text">{beer.tagline}</p>
                        <p className="card-text">{beer.abv}</p>
                    </div>
                </div>
            </div>)
    });

    return (
        <div className="row">
            {myBeers}
        </div>
    )
}

export default BeersList
