import React, { useState, useEffect } from 'react';
import BeersList from './beerslist';
import RangeBeers from './rangbeers';

function Beers({myRange, changeRange}) {

    const [range, setRange] = useState(myRange);
    const [data, setData] = useState([]);
    const [beers, setBeers] = useState([]);
    const [status, setStatus] = useState('idle');

    const myFetch = () => {
        if (status !== 'idle') return;

        const fetchData = async () => {
            console.log('Fetching...');
            setStatus('fetching');
            const response = await fetch(
                'https://api.punkapi.com/v2/beers'
            );
            const data = await response.json();
            setData(data);
            setBeers(data.filter((beer) => { return (beer.abv >= range[0] && beer.abv <= range[1]) }))
            setStatus('fetched');
        };

        fetchData();
    };

    useEffect(myFetch, [status]);

    const handleChange = (newValue) => {
        setRange(newValue);
        setBeers(data.filter((beer) => { return (beer.abv >= range[0] && beer.abv <= range[1]) }));
        changeRange(range);
    };

    return (
        <div className="container">
            <RangeBeers handleRange={handleChange} range={range}></RangeBeers>
            <BeersList beers={beers}></BeersList>
        </div>

    )
}

export default Beers


