import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from "react-datepicker";

export default class DateApod extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            selectedDate : new Date()
        }
    }

    setStartDate(date) {
        const stringDate = moment(date).format('YYYY-MM-DD');
        console.log(stringDate);
        this.setState({selectedDate: date});
        this.props.selectedDate(stringDate);
    }
    
    render() {
        return (
            <DatePicker selected={this.state.selectedDate} onChange={date => {this.setStartDate(date)}} />
        )
    }
}
