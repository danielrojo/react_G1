import React, { Component } from 'react';
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import DateApod from './dateapod';
import DisplayApod from './displayapod';

export default class Apod extends Component {
    constructor(props) {
        super(props)

        this.state = {
            apod: {},
            stringDate : moment(new Date()).format('YYYY-MM-DD')
        }
    }

    componentDidMount() {
        // this.doRequest();
    }



    setStartDate(date) {
        const stringDate = moment(date).format('YYYY-MM-DD');
        console.log(stringDate);
        this.setState({selectedDate: date});
        // this.doRequest(stringDate);
    }


    render() {

        return (
            <div className="container">
                <DateApod selectedDate={(date)=>{this.setState({stringDate: date})}}></DateApod>
                <DisplayApod date={this.state.stringDate}/>
            </div>
        )
    }
}
