import React, { Component } from 'react';
import ReactPlayer from 'react-player';


export default class DisplayApod extends Component {

    constructor(props) {
        super(props)

        this.state = {
            apod: {}
        }
    }

    doRequest(date){
        let url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
        if(date !== undefined) {
            url = url + '&date=' + date;
        }
        fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(JSON.stringify(data));
            this.setState({ apod: data });
            this.didMount = false;
        })
    }

    componentDidMount() {
        this.didMount = true;
        this.doRequest(this.props.date)
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.date !== prevProps.date || this.didMount){
            this.doRequest(this.props.date);
            
        }
    }

    render() {
        let content = <div></div>;
        if (this.state.apod.media_type === "image") {
            content = <img src={this.state.apod.url} className="mx-auto d-block" alt=""></img>
        } else if (this.state.apod.media_type === "video") {
            content = <ReactPlayer url={this.state.apod.url} className="mx-auto d-block"/>
        }
        return (
            <div className="jumbotron">
                <h1 className="display-3">{this.state.apod.title}</h1>
                <p className="lead">{this.state.apod.date}</p>
                <hr className="my-2" />
                {content}
                <p className="mt-2">{this.state.apod.explanation}</p>
                <p className="lead">
                    <a className="btn btn-primary btn-lg" href={this.state.apod.hdurl} role="button">Imagen HD</a>
                </p>
            </div>
        )
    }
}
