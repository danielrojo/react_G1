const reducer = (
  state = {
    beers: {
      range: [0, 6]
    },
    sw: {
      heroes: [],
      next: 'https://swapi.dev/api/people'
    },
    heroes: {
      heroList: [
        { name: 'Batman', description: 'dark knight' },
        { name: 'Superman', description: 'man of steel' }],
      newHero: {
        name: '',
        description: ''
      }
    },
    calculator:{
      calculatorState: 0,
      firstFigure: 0,
      secondFigure: 0,
      result: 0,
      operator: '',
      display: ''
    }
  }, action) => {
  switch (action.type) {
    case "UPDATE_CALCULATOR":
      state.calculator = action.payload.engine;
      return state;

    case "UPDATE_RANGE":
      state.beers.range = action.payload.range;
      return state;

    case "UPDATE_CHARACTERS":
      state.sw.heroes = action.payload.heroes;
      state.sw.next = action.payload.next;
      return state;

    case "ADD_HERO":
      state.heroes.heroList = [...state.heroes.heroList, action.payload.hero];
      return state;

    case "REMOVE_HERO":
      let temp = state.heroes.heroList;
      temp.splice(action.payload.index, 1);
      state.heroes.heroList = temp;
      return state;

    case "UPDATE_NEW_HERO":
      state.heroes.newHero = action.payload.newHero;
      return state;

    default:
      return state;
  }
};

export default reducer;