import Heroes from "../components/heroes/heroes";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    heroes: state.heroes.heroList,
    newHero: state.heroes.newHero
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addHero: newHero => {
      dispatch({ type: "ADD_HERO", payload: { hero: newHero } } );
    },
    updateHero: hero => {
      dispatch({ type: "UPDATE_NEW_HERO", payload: { newHero: hero } } );
    },
    removeHero: i => {
      dispatch({ type: "REMOVE_HERO", payload: { index: i } } );
    }

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);