import Beers from "../components/beers/beers";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    myRange: state.beers.range
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeRange: range => {
      
      dispatch({ type: "UPDATE_RANGE", payload: { range: range } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Beers);