import SwComponent from "../components/sw/swcomponent";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    myHeroes: state.sw.heroes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateHeroes: myHeroes => {
      dispatch({ type: "UPDATE_CHARACTERS", payload: { heroes: myHeroes.heroes, next: myHeroes.next } } );
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SwComponent);