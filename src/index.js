// CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

// Imports
import React from 'react';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import { render } from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import reducer from "./reducers/reducer";

// Components
import Apod from './components/apod/apod';

// Containers
import Calculator from './containers/calculatorcontainer';
import Heroes from './containers/heroescontainer';
import Beers from "./containers/beerscontainer";
import SwComponent from './containers/heroescontainer';


const store = createStore(reducer);

render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <div>
          <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <a className="navbar-brand" href="http://localhost:3000">React</a>
            <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavId">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" activeClassName="active" to="/calculator">Calculadora</NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/heroes">Héroes </NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/apod">Apod </NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/beers">Cervezas </NavLink>
                </li>
                <li>
                  <NavLink className="nav-link" to="/sw">Star Wars </NavLink>
                </li>
              </ul>
            </div>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/calculator">
              <Calculator />
            </Route>
            <Route path="/heroes">
              <Heroes />
            </Route>
            <Route path="/apod">
              <Apod />
            </Route>
            <Route path="/beers">
              <Beers />
            </Route>
            <Route path="/sw">
              <SwComponent />
            </Route>
            <Route path="/">
              <SwComponent />
            </Route>
            <Route path="*">
              <h1>Error en el path</h1>
            </Route>

            
          </Switch>
        </div>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
